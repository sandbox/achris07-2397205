Paguelofacil API
================
Commerce Paguelofacil is a Paguelofacil Onsite integration for the Drupal Commerce payment system.

It's based on the [Authorize.net] Drupal commerce module.

Implements the following:
  - Credit card payment and integration with the Commerce checkout form
  - Easy API configuration through the Rules module

For more information on Paguelofacil, check the following URL: http://paguelofacil.com/


Installation
============
Make sure to install the Paguelofacil module on the site. Once installed, the payment method can be enabled in the Rules interface (Configuration -> Workflow -> Rukes), under the rule Paguelofacil - On site.


Configuration
=============
By editing the rule action, the module configuration can be modified. The following configurations are required for the module to work:
- Key (CCLW)
- Company name
- Logo
- Accredit to
- Return URL

The first four are to be provided by Paguelofacil, while the fifth configuration is either an internal or external URL to be redirect to, after the payment is processed. If you have any question or doubts about the payment process or the required parameters, please contact the payment service provider.

The module also allows to configure the transaction mode. "Developer test account transactions" implements the module by registering the transaction in the system, but the payment request is not sent to the gateway; "Test transactions in a live account" sends a payment request during a transaction to a sandbox account; "Live transactions in a live account" sends a payment request to a live account. 

The first two transactions modes are to be used for developing purposes, while the the third mode is to be set for production sites.

Finally, the following configurations can be set:
- Credit card types to allow (as of the writing of this file, only Visa and Mastercard are allowed by the provider, any other will fail during the payment)
- Logging options (whether to log either request or response messages)
